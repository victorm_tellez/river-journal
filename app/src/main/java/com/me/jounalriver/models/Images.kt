package com.me.jounalriver.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Víctor Téllez on 27/11/2017.
 */
class Images(@SerializedName("thumbnail")val thumbnail: Thumbnail)