package com.me.jounalriver.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Víctor Téllez on 27/11/2017.
 */
class All(@SerializedName("rendered")val rendered: String,
              @SerializedName("status") val status: Boolean,
            @SerializedName("response") val articles: Articles
)