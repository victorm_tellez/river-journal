package com.me.jounalriver.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Víctor Téllez on 15/09/2017.
 */
class Articles(@SerializedName("articles") val articles: List<Article>)