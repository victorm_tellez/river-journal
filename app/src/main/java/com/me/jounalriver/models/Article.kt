package com.me.jounalriver.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Víctor Téllez on 27/11/2017.
 */
class Article(@SerializedName("title") val title: String,
              @SerializedName("images") val images: Images)