package com.me.jounalriver.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.me.jounalriver.R;
import com.me.jounalriver.models.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Víctor Téllez on 27/11/2017.
 */

public class RiverAdapter extends RecyclerView.Adapter<RiverAdapter.ViewHolder> {

    private List<Article> articles;

    public RiverAdapter(List<Article> items) {
        this.articles = items;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        Article article = articles.get(position);
        holder.text.setText(article.getTitle());
        holder.image.setImageBitmap(null);
        Picasso.with(holder.image.getContext()).cancelRequest(holder.image);
        Picasso.with(holder.image.getContext()).load(article.getImages().getThumbnail().getUrl()).into(holder.image);
        holder.itemView.setTag(article);
    }

    @Override public int getItemCount() {
        return articles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.article_title);
            image = (ImageView) itemView.findViewById(R.id.article_image);
        }
    }
}