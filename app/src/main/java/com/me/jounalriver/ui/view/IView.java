package com.me.jounalriver.ui.view;

import android.content.Context;

/**
 * Created by Víctor Téllez on 27/11/2017.
 */

public interface IView {
    Context getActivity();
}
