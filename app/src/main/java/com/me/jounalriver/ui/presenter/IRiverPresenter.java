package com.me.jounalriver.ui.presenter;

/**
 * Created by Víctor Téllez on 27/11/2017.
 */

public interface IRiverPresenter {
    void getArticlesByPublication(String publication);
    void getArticlesByTag(String tag);
}
