package com.me.jounalriver.ui.presenter;

import android.util.Log;

import com.me.jounalriver.api.RiverRetriever;
import com.me.jounalriver.models.All;
import com.me.jounalriver.ui.view.IRiverView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Víctor Téllez on 27/11/2017.
 */

public class RiverPresenter implements IRiverPresenter {

    public static final String RIVER_HOME = "thejournal";
    public static final String RIVER_TAG = "";

    private IRiverView view;
    private RiverRetriever retriever2;

    public RiverPresenter(IRiverView view) {
        this.view = view;
        retriever2 = new RiverRetriever();
        retriever2.auth();
    }

    @Override
    public void getArticlesByPublication(String publication) {
        retriever2.getArticleByPublication(new Callback<All>() {
            @Override
            public void onResponse(Call<All> call, Response<All> response) {
                Log.d("Articles", "call:" + call+ " response:" + response);
                if (call.isExecuted()) {
                    Log.d("Articles", "Articles:" + response.body());
                    view.setArticles(response.body().getArticles());
                }
            }

            @Override
            public void onFailure(Call<All> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    @Override
    public void getArticlesByTag(String tag) {
        retriever2.getArticleByTag(new Callback<All>() {
            @Override
            public void onResponse(Call<All> call, Response<All> response) {
                Log.d("Articles", "call:" + call+ " response:" + response);
                if (call.isExecuted()) {
                    Log.d("Articles", "Articles:" + response.body());
                    view.setArticles(response.body().getArticles());
                }
            }

            @Override
            public void onFailure(Call<All> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }
}
