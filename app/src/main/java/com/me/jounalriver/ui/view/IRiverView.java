package com.me.jounalriver.ui.view;


import com.me.jounalriver.models.Articles;

/**
 * Created by Víctor Téllez on 27/11/2017.
 */

public interface IRiverView extends IView {

    void setArticles(Articles articles);
}
