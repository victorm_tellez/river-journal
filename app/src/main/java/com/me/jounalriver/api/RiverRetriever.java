package com.me.jounalriver.api;

import com.me.jounalriver.models.All;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Víctor Téllez on 27/11/2017.
 */

public class RiverRetriever {

    private RiverAPI service;

    public void auth() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request originalRequest = chain.request();

                Request.Builder builder = originalRequest.newBuilder().header("Authorization",
                        Credentials.basic("sample", "eferw5wr335£65", Charset.forName("UTF-8")));

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.thejournal.ie/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        service = retrofit.create(RiverAPI.class);
    }

    public void getArticleByPublication(Callback<All> callback) {
        Call call = service.getPublications(Credentials.basic("sample","eferw5wr335£65", Charset.forName("UTF-8")));
        call.enqueue(callback);
    }

    public void getArticleByTag(Callback<All> callback) {
        Call call = service.getTags(Credentials.basic("sample","eferw5wr335£65", Charset.forName("UTF-8")));
        call.enqueue(callback);
    }
}
