package com.me.jounalriver.api

import com.me.jounalriver.models.All
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

/**
 * Created by Víctor Téllez on 14/09/2017.
 */
interface RiverAPI {

   @Headers("Content-Type: application/json")
   @GET("v3/sample/thejournal/response/articles/")
   fun getPublications(@Header("Authorization") credentials: String) : Call<All>

   @Headers("Content-Type: application/json")
   @GET("v3/sample/tag/google")
   fun getTags(@Header("Authorization") credentials: String) : Call<All>

}